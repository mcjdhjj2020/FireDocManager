package com.dullong.firedocmanager.Controller.test;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UploadTest {

	@ResponseBody
	@RequestMapping(value = "/test/upload.json", method = RequestMethod.POST)
	public String upload(
			@RequestParam(value = "qqfile", required = true) MultipartFile file,
			HttpServletRequest request, HttpServletResponse response) {
		if (null == file) {
			return "{\"success\": false}";
		}

		System.out.println("上传名字为：" + file.getOriginalFilename());

		return "{\"success\": true,\"docid\": \"123\",\"docname\": \""
				+ file.getOriginalFilename() + "\"}";
	}

	public static void main(String[] s) {
		File f = new File("D:/123/123.txt");
		System.out.println(f.getName());
		System.out.println(f.getPath());
		System.out.println(f.getParent());
		System.out.println(f.getAbsolutePath());
		
	}

}
