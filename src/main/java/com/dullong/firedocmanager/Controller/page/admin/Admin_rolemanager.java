package com.dullong.firedocmanager.Controller.page.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dullong.firedocmanager.bean.Cate;
import com.dullong.firedocmanager.bean.Role;
import com.dullong.firedocmanager.service.CateService;
import com.dullong.firedocmanager.service.RoleService;
import com.dullong.firedocmanager.service.base.BaseService;
import com.dullong.firedocmanager.util.json.JsonUtils;
import com.dullong.firedocmanager.util.json.SendJsonUtil;

@Controller
public class Admin_rolemanager {

	@Autowired
	RoleService roleService;
	@Autowired
	BaseService baseService;

	@RequestMapping("/admin/role.html")
	public ModelAndView listAllCate(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView();
		List<Role> listByParent = roleService.listRootRole();
		List<Role> listAll = roleService.listAll();
		view.addObject("rolelist", listAll);
		processRoleTree(listByParent);
		view.addObject("roletree", listByParent);
		view.setViewName("admin/role");

		return view;
	}

	@RequestMapping("/admin/role-u.html")
	public void getRolesByUserID(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "username", required = false) String username) {
		if (null == username || "".equals(username)) {
			username = baseService.getUser().getUserName();
		}
		List<Role> roles = roleService.getByUser(username);

		SendJsonUtil.sendJson(JSONArray.fromObject(roles).toString(), response);
	}

	@RequestMapping("/admin/role-u-all.html")
	public void getAllRolesWithStatByUserID(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "username", required = false) String username) {
		if (null == username || "".equals(username)) {
			username = baseService.getUser().getUserName();
		}
		List<Map<String, Object>> withStatByUser = roleService
				.getWithStatByUser(username);

		SendJsonUtil.sendJson(JSONArray.fromObject(withStatByUser).toString(),
				response);
	}

	@RequestMapping("/admin/addrole.html")
	public ModelAndView addRole(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam(value = "id", required = false) Integer id,
			@RequestParam(value = "parent", required = false) Integer parent,
			@RequestParam(value = "option", required = true) Integer option,
			@RequestParam(value = "title", required = true) String title) {
		ModelAndView view = new ModelAndView();

		if (option == 0) {
			parent = parent == -1 ? null : parent;
			roleService.save(title, parent);
		} else if (option == 1) {
			roleService.update(title, parent, id);
		} else if (option == 2) {
			roleService.delete(id);
		}

		view.setViewName("redirect:/admin/role.html");
		return view;
	}

	@RequestMapping("/admin/role-{id}.html")
	public void listRoleByParent(HttpServletRequest request,
			HttpServletResponse response, @PathVariable Integer id) {
		List<Role> listByParent = roleService.listByParent(id);
		JsonUtils jsonUtils = new JsonUtils();
		JsonUtils putPropertiy = jsonUtils.putPropertiy("data", listByParent);
		try {
			SendJsonUtil.sendJson(putPropertiy.writeToString(), response);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void processRoleTree(List<Role> listByParent) {
		if (listByParent != null) {
			for (Role c : listByParent) {
				Integer id = c.getId();
				List<Role> listByParent2 = roleService.listByParent(id);
				c.setChilds(listByParent2);
				processRoleTree(listByParent2);
			}
		}
	}
}
