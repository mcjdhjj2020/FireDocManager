package com.dullong.firedocmanager.Controller.page.admin;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Admin_homepage {

	@RequestMapping(value={"/admin","/admin/home.html"})
	public ModelAndView environment(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView();
		Properties props = System.getProperties();
		Runtime runtime = Runtime.getRuntime();
		long freeMemory = runtime.freeMemory();
		long totalMemory = runtime.totalMemory();
		long maxMemory = runtime.maxMemory();
		long usedMemory = totalMemory - freeMemory;
		long useableMemory = maxMemory - totalMemory + freeMemory;
		int div = 1000;
		double freeMemoryMB = ((double) freeMemory) / div / div;
		double totalMemoryMB = ((double) totalMemory) / div / div;
		double usedMemoryMB = ((double) usedMemory) / div / div;
		double maxMemoryMB = ((double) maxMemory) / div / div;
		double useableMemoryMB = ((double) useableMemory) / div / div;
		view.addObject("props", props);
		view.addObject("maxMemoryMB", maxMemoryMB);
		view.addObject("usedMemoryMB", usedMemoryMB);
		view.addObject("useableMemoryMB", useableMemoryMB);
		view.addObject("totalMemoryMB", totalMemoryMB);
		view.addObject("freeMemoryMB", freeMemoryMB);
		view.setViewName("admin/admin");
		return view;
	}
	
	@RequestMapping(value={"/admin/systeminfo.html"})
	public ModelAndView systeminfo(HttpServletRequest request,
			HttpServletResponse response) {
		ModelAndView view = new ModelAndView();
		
		view.setViewName("admin/systeminfo");
		return view;
	}
	
}
