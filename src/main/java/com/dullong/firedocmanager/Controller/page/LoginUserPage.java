package com.dullong.firedocmanager.Controller.page;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dullong.firedocmanager.bean.User;
import com.dullong.firedocmanager.service.UserService;
import com.dullong.firedocmanager.service.base.BaseService;

@Controller
@RequestMapping("/admin/user/")
public class LoginUserPage {

	@Autowired
	UserService userService;
	@Autowired
	BaseService baseService;

	@RequestMapping(value = "{path}.html", method = RequestMethod.GET)
	public ModelAndView searchPage(HttpServletRequest request,
			HttpServletResponse response, @PathVariable String path)
			throws IOException {
		ModelAndView view = new ModelAndView();
		User user = baseService.getUser();
		view.addObject("u", user);
		view.setViewName("admin/user/" + path);
		return view;
	}

}
