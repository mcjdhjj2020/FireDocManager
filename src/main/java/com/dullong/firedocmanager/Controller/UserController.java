package com.dullong.firedocmanager.Controller;

import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dullong.firedocmanager.bean.User;
import com.dullong.firedocmanager.service.UserService;
import com.dullong.firedocmanager.util.json.SendJsonUtil;

@Controller
@RequestMapping("/logon")
public class UserController {

	@Autowired
	UserService userService;

	@ResponseBody
	@RequestMapping("/logon.html")
	public void logon(HttpServletRequest request, HttpServletResponse response,
			String username, String psw) {
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		User u = userService.getUserByUP(username, psw);
		if (u == null) {
			SendJsonUtil.sendJson("{\"success\": false,\"message\": \""
					+ username + "不存在\"}", response);
			return;
		}
		SendJsonUtil.sendJson("{\"success\": true,\"message\": \"" + username
				+ "登陆成功\"}", response);
	}

	@ResponseBody
	@RequestMapping("/register.html")
	public void register(HttpServletRequest request,
			HttpServletResponse response, String username, String psw,
			String email) {
		if (userService.isExist(username, email)) {
			SendJsonUtil.sendJson("{\"success\": false,\"message\": \""
					+ username + "或" + email + "已存在\"}", response);
			return;
		}
		User u = new User();
		u.setEmail(email);
		u.setUserName(username);
		u.setPSW(psw);
		
		u.setLastOnline(100000000l);
		String save = userService.save(u);
		if (username.equals(save)) {

			SendJsonUtil.sendJson("{\"success\": true}", response);
			return;
		}
		SendJsonUtil.sendJson("{\"success\": false,\"message\": \"" + username
				+ "注册失败\"}", response);
	}

	@ResponseBody
	@RequestMapping("/isexist.html")
	public String isExist(HttpServletRequest request,
			HttpServletResponse response, String username, String email) {
		return userService.isExist(username, email) + "";
	}
}
