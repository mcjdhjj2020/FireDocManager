package com.dullong.firedocmanager.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dullong.firedocmanager.service.DocumentService;

@Controller
public class DocConverController {

	@Autowired
	DocumentService documentService;

	@ResponseBody
	@RequestMapping(value = "/utils/docConvert_{ID}.json")
	public String convert(HttpServletRequest request,
			HttpServletResponse response, @PathVariable Long ID) {
		boolean convert = documentService.convert(ID);
		return "{\"success\": " + convert + "}";
	}

	@ResponseBody
	@RequestMapping(value = "/utils/docParser_{ID}.json")
	public String parserBody(HttpServletRequest request,
			HttpServletResponse response, @PathVariable Long ID) {
		boolean isTrue = documentService.parserBody(ID);
		return "{\"success\": " + isTrue + "}";
	}

}
