package com.dullong.firedocmanager.JODConverter;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.Resource;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.DocumentFormat;
import com.artofsolving.jodconverter.DocumentFormatRegistry;

@Resource
public class ConverterImpl implements Converter {

	public DocumentConverter converter;
	public DocumentFormatRegistry registry;

	public ConverterImpl(DocumentConverter converter,
			DocumentFormatRegistry registry) {
		super();
		this.converter = converter;
		this.registry = registry;
	}

	@Override
	public void convert(File inputFile, File outputFile) {

		DocumentFormat inputFormat = registry
				.getFormatByFileExtension(FilenameUtils.getExtension(inputFile
						.getAbsolutePath()));
		DocumentFormat outputFormat = registry
				.getFormatByFileExtension(FilenameUtils.getExtension(outputFile
						.getAbsolutePath()));
		converter.convert(inputFile, inputFormat, outputFile, outputFormat);

	}

	@Override
	public void convert(File inputFile, DocumentFormat inputFormat,
			File outputFile, DocumentFormat outputFormat) {
		converter.convert(inputFile, inputFormat, outputFile, outputFormat);

	}

}
