package com.dullong.firedocmanager.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.dullong.firedocmanager.bean.User;

public class LoginAnnotationInterceptor extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession();
		// 取得session中的用户信息, 以便判断是否登录了系统
		User u = (User) session.getAttribute("user");
		if (null == u) {
			// 需要登录

			response.sendRedirect(request.getContextPath()
					+ "/logon.html?oprst=false&opmsg=请登录!");
			return false;
		}
		return true;
	}
}
