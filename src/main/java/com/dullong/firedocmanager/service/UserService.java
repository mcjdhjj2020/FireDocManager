package com.dullong.firedocmanager.service;

import java.util.List;

import com.dullong.firedocmanager.bean.User;
import com.dullong.firedocmanager.service.base.BaseService;

public interface UserService {

	public User getUserById(String userId);

	public User getUserByEmail(String email);

	public User getUserByUsername(String username);

	public boolean isExist(String username, String email);

	public User getUserByUP(String username, String psw);

	public String save(User user);

	public String save(User user, Integer... role);

	public boolean addRole(String username, Integer role);

	public boolean removeRole(String username, Integer role);

	public String update(String sql, Object... obj);

	public List<User> getAllUsers();

	public boolean delete(String username);
}
