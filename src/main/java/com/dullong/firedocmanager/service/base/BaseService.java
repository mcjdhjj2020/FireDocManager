package com.dullong.firedocmanager.service.base;

import java.io.Serializable;

import com.dullong.firedocmanager.bean.User;

public interface BaseService extends Serializable {

	public User getUser();
}
