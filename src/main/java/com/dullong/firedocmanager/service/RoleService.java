package com.dullong.firedocmanager.service;

import java.util.List;
import java.util.Map;

import com.dullong.firedocmanager.bean.Role;

public interface RoleService {

	public Integer save(String title, Integer parentid);

	public Integer update(String title, Integer parentid, Integer id);

	public Integer delete(Integer id);

	public Role get(Integer id);

	public List<Role> getByUser(String username);

	public List<Map<String, Object>> getWithStatByUser(String username);

	public List<Role> listByParent(Integer parent);

	public List<Role> listRootRole();

	public List<Role> listAll();
}
