package com.dullong.firedocmanager.service;

import java.util.List;

import com.dullong.firedocmanager.bean.Cate;
import com.dullong.firedocmanager.service.base.BaseService;

public interface CateService {

	public Integer save(String title, Integer parentid);

	public Integer update(String title, Integer parentid, Integer id);

	public Integer delete(Integer id);

	public Cate get(Integer id);

	public List<Cate> listByParent(Integer parent);

	public List<Cate> listRootCate();

	public List<Cate> listAll();
}
