package com.dullong.firedocmanager.itext;

import java.util.Map;

public interface PDFReader {

	public Map<String, Object> getPdfFileText(String filename);
}
