package com.dullong.firedocmanager.lucene4.domain;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.dullong.firedocmanager.lucene4.Searchable;

public class LDocument implements Searchable {

	Long id;
	String title;
	Integer page;
	String type;
	Long uploadtime;
	Integer cate;
	String body;
	Integer roleid;

	@Override
	public int compareTo(Searchable o) {
		return 0;
	}

	@Override
	public long id() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public List<String> storeFields() {
		return Arrays.asList("title", "type", "uploadtime", "page", "cate",
				"roleid");
	}

	@Override
	public List<String> indexFields() {
		return Arrays.asList("title", "body", "type");
	}

	@Override
	public float boost() {
		return 1.0f;
	}

	@Override
	public Map<String, String> extendStoreDatas() {
		return null;
	}

	@Override
	public Map<String, String> extendIndexDatas() {
		return null;
	}

	@Override
	public List<? extends Searchable> ListAfter(long id, int count) {
		return null;
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getUploadtime() {
		return uploadtime;
	}

	public void setUploadtime(Long uploadtime) {
		this.uploadtime = uploadtime;
	}

	public Integer getCate() {
		return cate;
	}

	public void setCate(Integer cate) {
		this.cate = cate;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

}
