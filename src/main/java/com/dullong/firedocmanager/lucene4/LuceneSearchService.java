package com.dullong.firedocmanager.lucene4;

import java.io.IOException;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.dullong.firedocmanager.bean.PDocument;
import com.dullong.firedocmanager.lucene4.domain.QDocument;
import com.dullong.firedocmanager.util.BeanDebugger;
import com.mysema.query.lucene.LuceneQuery;
import com.mysema.query.types.Predicate;
import com.mysema.query.types.expr.BooleanExpression;

@Service
@Scope("singleton")
public class LuceneSearchService extends LuceneBase {
	QDocument doc = new QDocument("doc");

	public LuceneSearchService() {

	}

	public PDocument search(String title_keyword, String body_keyword,
			Integer title_body, Long uploadtimeF, Long uploadtimeT,
			String[] types, Integer[] cates, Integer[] roleids, Long page,
			Long limit) throws IOException {
		BooleanExpression keyword_Expression;
		BooleanExpression uploadtime_Expression;
		BooleanExpression type_Expression = null;
		BooleanExpression cate_Expression = null;
		BooleanExpression role_Expression = null;
		System.out.println("---------------------------------------title_body:"+title_body);
		if (title_body == 1) {
			keyword_Expression = doc.title.contains(title_keyword).and(
					doc.body.contains(body_keyword));
		} else {
			keyword_Expression = doc.title.contains(title_keyword).or(
					doc.body.contains(body_keyword));
		}

		uploadtime_Expression = doc.uploadtime
				.between(uploadtimeF, uploadtimeT);

		if (types != null && types.length >= 1) {
			BooleanExpression eq = doc.type.eq(types[0]);
			type_Expression = doOrType(eq, types);
		}

		if (cates != null && cates.length >= 1) {
			BooleanExpression eq = doc.cate.eq(cates[0]);
			cate_Expression = doOrCate(eq, cates);
		}

		if (roleids != null && roleids.length >= 1) {
			BooleanExpression eq = doc.roleid.eq(roleids[0]);
			role_Expression = doOrRole(eq, roleids);
		}

		return doSearch(title_keyword, page, limit, keyword_Expression,
				uploadtime_Expression, type_Expression, cate_Expression,
				role_Expression);
	}

	private PDocument doSearch(String title_keyword, Long page, Long limit,
			Predicate... e) throws IOException {
		LuceneQuery query = new LuceneQuery(getIndexSearcher());
		List<org.apache.lucene.document.Document> list = query.where(e)
				.offset((page - 1) * limit).limit(limit).list();
		long count = query.count();
		System.out.println("count:" + count);

		System.out.println("----------------"+query.toString());
		System.out.println("------------------"+list.size());
		for (org.apache.lucene.document.Document d : list) {
			System.out.println(d.get("ID") + ":" + d.get("page") + ":"
					+ d.get("title"));
			System.out.println("-----------------"+d.toString());
			BeanDebugger.dump(d);
		}
		return LD2PD(list, count, page, limit, title_keyword);
	}

	private BooleanExpression doOrCate(BooleanExpression eq, Integer[] cates) {
		for (int i = 1; i < cates.length; i++)
			eq = eq.or(doc.cate.eq(cates[i]));
		return eq;
	}

	private BooleanExpression doOrType(BooleanExpression eq, String[] types) {
		for (int i = 1; i < types.length; i++)
			eq = eq.or(doc.type.eq(types[i]));
		return eq;
	}

	private BooleanExpression doOrRole(BooleanExpression eq, Integer[] roleids) {
		for (int i = 1; i < roleids.length; i++)
			eq = eq.or(doc.roleid.eq(roleids[i]));
		return eq;
	}
}
