package com.dullong.firedocmanager.bean;

import java.util.List;

public class PDocument {

	private List<Document> documents;
	private PageBean pageBean;
	
	

	public PDocument(List<Document> documents, PageBean pageBean) {
		super();
		this.documents = documents;
		this.pageBean = pageBean;
	}

	public List<Document> getDocuments() {
		return documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public PageBean getPageBean() {
		return pageBean;
	}

	public void setPageBean(PageBean pageBean) {
		this.pageBean = pageBean;
	}

}
