package com.dullong.firedocmanager.util.json;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

public class Jacksons {
	private ObjectMapper objectMapper;
	private Map<String, Object> map = new HashMap<String, Object>();

	public static Jacksons me() {
		return new Jacksons();
	}

	public Jacksons putProperty(String name, Object value) {
		map.put(name, value);
		return this;
	}

	private Jacksons() {
		objectMapper = new ObjectMapper();
		// 设置输入时忽略在JSON字符串中存在但Java对象实际没有的属性
		objectMapper
				.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
		objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS,
				false);

		objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
	}

	private SimpleFilterProvider simpleFilterProvider = new SimpleFilterProvider();

	public Jacksons filter(String filterName, String... properties) {
		FilterProvider filterProvider = simpleFilterProvider.addFilter(
				filterName,
				SimpleBeanPropertyFilter.serializeAllExcept(properties));
		objectMapper.setFilters(filterProvider);
		return this;
	}

	// 混入注解
	public Jacksons addMixInAnnotations(Class<?> target, Class<?> mixinSource) {
		objectMapper.getSerializationConfig().addMixInAnnotations(target,
				mixinSource);
		objectMapper.getDeserializationConfig().addMixInAnnotations(target,
				mixinSource);
		return this;
	}

	public Jacksons setDateFormate(DateFormat dateFormat) {
		objectMapper.setDateFormat(dateFormat);
		return this;
	}

	public <T> T json2Obj(String json, Class<T> clazz) {
		try {
			return objectMapper.readValue(json, clazz);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("解析json错误");
		}
	}

	public String readAsString(Object obj) {
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("解析对象错误");
		}
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> json2List(String json) {
		try {
			return objectMapper.readValue(json, List.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("解析json错误");
		}
	}

	public String write2String() {
		try {
			return objectMapper.writeValueAsString(map);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("解析json错误");
		}
	}

	public String writeFailJson(String message) {
		map.put("success", false);
		map.put("message", message);
		return write2String();
	}

	public String writeSuccessJson(Object message) {
		map.put("success", true);
		if (message != null)
			map.put("message", message);
		return write2String();
	}
}
