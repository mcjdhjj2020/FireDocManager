<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!doctype html>
<html basepath='<%=basePath%>'>

 <head>
  <base href="<%=basePath%>">
  <meta charset="utf-8" />
  <title>search page</title>
  <!-- basic styles -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>


  <link href="<c:out value="${smvc }" />/assets/css/bootstrap.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="<c:out value="${smvc }" />/assets/css/font-awesome.min.css" />

  <!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

  <style>
body {
	background-color: #fbfaf8;
}  /* #fbfaf8  */
ul,li {
	list-style: none;
}

a,a:hover {
	text-decoration: none;
}

.header {
	font-family: 微软雅黑, 'Microsoft Yahei', 'Hiragino Sans GB', tahoma, arial,
		宋体;
	z-index: 999;
	margin: 0;
	padding: 0;
}

.nav1 a,.nav1 ul,.nav1 li,.nav1 h2 {
	color: #fff;
}

.nav1 h2 {
	color: #fff;
	font-size: 16px;
	margin-top: 30px;
}

.header-search-outer {
	margin-top: 20px;
	background-color: #008fb5;
}

.header-search {
	background-color: #008fb5;
	border: 0px solid #ccc;
	color: #fff;
}

.nav1 div {
	display: inline-block;
}

.topbar-right {
	float: right;
	margin-right: 50px;
}

.header ul {
	list-style: none;
	display: inline-block;
}

.header li {
	list-style: none;
	display: inline-block;
}

#J_Menu li:hover {
	background-color: #0095bb;
}

#J_Menu li {
	padding: 0 14px;
	height: 75px;
	line-height: 75px;
	font-size: 16px;
}

/* 以上为header */
.main-content {
	background-color: #fff;
	border: 1px solid #eee;
	padding: 0;
	margin-bottom: 5px;
}

.main-content .region-content a {
	color: #00a2ca;
}

.main-content .region-content a:hover {
	color: #017CB9;
}

.main-content .region-content .node-header {
	position: relative;
	margin-bottom: 10px;
}

.main-content .region-content .article-type {
	position: absolute;
	left: 0;
	top: -30px;
	width: 50px;
	height: 50px;
}

.main-content .region-content .article-type i {
	position: absolute;
	width: 50px;
	height: 50px;
	background-color: #3ed4e5;
	z-index: 10;
	color: #FFFFFF;
	font-size: 20px;
	line-height: 50px;
	text-align: center;
}

.main-content .region-content .title-bottom {
	color: #cccccc;
	margin-left: 60px;
}

.main-content .region-content .node-teaser footer a {
	color: #cccccc;
}

.main-content .region-content h1 {
	font-size: 22px;
	line-height: 36px;
	margin-left: 60px;
	margin-bottom: 0px;
}

.main-content .region-content ul,.main-content .region-content ul li {
	list-style: none;
	border: 0;
	padding: 0;
	display: inline-block;
}

article {
	padding: 20px 20px 20px 20px;
	line-height: 2;
}

article:hover {
	border: 1px solid #9cddf5 !important;
  padding: 19px 19px 19px 19px;
}

article .field {
	display: inline-block;
}

article .node_read_more {
	position: relative;
	right: 0;
	top: 0;
	float: right;
}
.highlight{
 color: red;
}

/* 以上为搜索结果区 */
.pager-item-list {
	text-align: center;
	border-top: 1px solid #00a3cf;
	padding-top: 10px;
	position: relative;
	margin: 50px 0 20px;
}

.pager-item-list:before,.pager-item-list:after {
	content: "";
	width: 10px;
	height: 10px;
	position: absolute;
	border-radius: 5px;
	background-color: #00a3cf;
	top: -5px;
}

.pager-item-list:before {
	left: 0;
}

.pager-item-list:after {
	right: 0;
}

.pagers {
	display: inline-block;
	padding-left: 0;
}

.pagers a {
	display: block;
	line-height: 26px;
	height: 26px;
	overflow: hidden;
	color: #333333;
}

.pagers li {
	width: 26px;
	height: 26px;
	line-height: 26px;
	border-radius: 50%;
	border: 1px solid #333333;
	color: #333333;
	text-align: center;
	float: left;
	position: relative;
	padding: 0;
	margin: 0 3px
}

.pagers .pager-last a:before,.pagers .pager-next a:before {
	display: block;
	width: 26px;
}

.pagers li:before {
	content: "";
	position: absolute;
	top: -11px;
	left: 50%;
	height: 10px;
	width: 1px;
	background-color: #333333;
}

.pagers .pager-current {
	border-color: #00a3cf;
	color: #00a3cf;
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	-webkit-transform-style: preserve-3d;
	-webkit-transform: rotateX(-180deg);
	-webkit-transform: rotateX(-180deg);
	-webkit-transform-origin: 0 -11px;
	-webkit-transform-origin: 0 -11px;
}

/* 以上为page列表区 */
.search-box {
	padding: 15px;
	padding: 15px;
	margin-bottom: 5px;
	background-color: #fff;
	-moz-box-shadow: 0px -4px 20px #9A9E8E;
	-webkit-box-shadow: 0px -4px 20px #9A9E8E;
	box-shadow: 0px -4px 20px #9A9E8E;
}

.search-box .nocurrent {
	display: none;
}

.search-box .current {
	display: block;
}

.search-box .search-type-list {
	display: inline-block;
	position: absolute;
	right: 20px;
	z-index: 100;
}

.search-box .ad-des {
	width: 250px;
	top: 10px;
	position: absolute;
	right: 20px;
	top: 80px;
	color: #cccccc;
}

/* 以上为搜索框 */
.widget-title {
	height: 32px;
	line-height: 34px;
	overflow: hidden;
	width: 116px;
	text-align: center;
	color: #fff;
	background: #3ed4e5;
	font-size: 14px;
	margin-bottom: 0;
}

.widget-content {
	border-top: 3px solid #3ed4e5;
	padding: 5px;
}

.widget-content ul {
	padding: 5px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	line-height: 22px;
	margin: 0;
}

.widget-content ul li {
	line-height: 28px;
	font-size: 14px;
	margin-top: 0px;
	color: #555;
}

.widget-content ul li a {
	color: #555;
}

/* 以上为右侧 */
.footer {
	margin-top: 5px;
	border-top: 1px;
	border-top-color: red;
	padding: 5px;
	position: relative;
	z-index: -1;
}

.copyright-100 {
	clear: both;
	background: #fff;
	border-top: 1px solid #e5e5e5;
}

.footer .copyright-100 .copyright {
	text-align: center;
	color: #999;
	font-size: 12px;
	padding: 35px 0 5px;
}

.copyright-100 .copyright p.big a {
	color: #666;
	padding: 0 5px;
	font-size: 14px;
}

.copyright-100 .copyright p a {
	padding: 0 5px;
	color: #b3b3b3;
}

#go_top {
	position: fixed;
	left: 100%;
	top: 420px;
	margin-left: -40px;
	width: 35px;
	height: 35px;
	line-height: 26px;
	border-radius: 50%;
	font-family: "w3cplus";
	text-align: center;
	font-size: 16px;
	background-color: #fff;
	border: 3px solid #3ed4e5;
	z-index: 99999999999;
}

#go_top:hover {
	background-color: #3ed4e5;
}

#go_top a:hover {
	color: #fff;
}
</style>


 </head>

 <body>
  <header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
   <div class="container">

    <div class="navbar-header">
     <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
     </button>
     <a href="" class="navbar-brand">FireDoc</a>
    </div>
    <nav class="navbar-collapse bs-navbar-collapse collapse" role="navigation" style="height: 1px;">
     <ul class="nav navbar-nav navbar-right">
      <li>
       <form action="search.html" class="" role="form" style="margin-top: 8px;">
        <input type="text" name="keyword" placeholder="搜索关键词" class="form-control" style="background-color: black;">
       </form>
      </li>
      <li>
       <a href="logon.html?logon=logon">登陆</a>
      </li>
      <li>
       <a href="logon.html?logon=reg">注册</a>
      </li>
      <li>
       <a href="admin/doc_add.html">上传文档</a>
      </li>
     </ul>

     <ul class="nav navbar-nav">
      
     </ul>
    </nav>

   </div>
  </header>
  <div class="clear" style="margin-top: 60px;"></div>

  <!-- 主内容区 -->
  <div class="container">
   <div class="row">
    <div class="col-md-9 main-content">