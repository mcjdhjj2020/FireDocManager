<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />

<jsp:include page="common-head.jsp"></jsp:include>


<div class="region region-content">

	<article id="node-1344"
		class="node node-blog node-promoted node-teaser clearfix">
	<h1 property="dc:title" datatype="">
		<c:out value="${document.title }" escapeXml="false"></c:out>
	</h1>

	<header class="node-header">
	<div class="title-bottom">

		<span class="icon-user"><c:out value="${document.uploaduser }"></c:out>
		</span>&nbsp;
		<span class="icon-calendar"><c:out
				value="${document.uploadtime }"></c:out> </span>&nbsp;
		<span class="icon-eye-open"><c:out value="${document.viewnum }"></c:out>
		</span>&nbsp;
		<span class="icon-download-alt"><c:out
				value="${document.downnum }"></c:out> </span>
	</div>
	<div class="article-type article-type-doc">
		<a href="" title="doc"> <i class="icon-blog"><c:out
					value="${document.type }"></c:out> </i> </a>
	</div>
	</header>

	<section class="body-content clearfix">
	<div
		class="field field-name-body field-type-text-with-summary field-label-hidden">
		<div class="field-items">
			<div class="field-item even" property="content:encoded">
				<p>
					<c:out value="${document.body }" escapeXml="false"></c:out>
				</p>
			</div>
		</div>
	</div>
	</section>


	<footer class="clearfix">
	<div
		class="field field-name-field-blog-tag field-type-taxonomy-term-reference field-label-hidden clearfix">
		<ul class="links">
			<i class="icon-tags" style="color: #cccccc;"></i>
			<li>
				<a href=""><c:out value="${document.roleid }"></c:out> </a>
			</li>
			<li>
				<a href=""><c:out value="${document.cate }"></c:out> </a>
			</li>
			<li>
				<a href="">计算机</a>
			</li>
			<li>
				<a href="">编程</a>
			</li>
		</ul>
	</div>
	<a href="javascript:void(0);" did="<c:out value='${document.ID }'/>"
		class="node_read_more" id="doc-down"
		dtype="<c:out value='${document.type }'/>" onclick="down()">下载 <i
		class="icon-download-alt"></i> </a>
	<script type="text/javascript" language="javascript">
	function down() {
		var a = $("#doc-down");
		var id = a.attr("did");
		var type = a.attr("dtype");
		console.log("id:" + id);
		console.log("type:" + type);
		var contextPath = $("html").attr("basepath");
		var href = contextPath + "/down/" + id + ".html";
		//var sFeatures = "height=600, width=810, scrollbars=yes, resizable=yes";
		//window.open(href, '3km', sFeatures);
		$.download(href,'type='+type,'get' );
		return false;
	}
	// Ajax 文件下载
	jQuery.download = function(url, data, method) {
		// 获得url和data
		if (url && data) {
			// data 是 string 或者 array/object
			data = typeof data == 'string' ? data : jQuery.param(data);
			// 把参数组装成 form的  input
			var inputs = '';
			jQuery
					.each(
							data.split('&'),
							function() {
								var pair = this.split('=');
								inputs += '<input type="hidden" name="'+ pair[0] +'" value="'+ pair[1] +'" />';
							});
			// request发送请求
			jQuery(
					'<form action="' + url + '" method="' + (method || 'post')
							+ '">' + inputs + '</form>').appendTo('body')
					.submit().remove();
		}
		;
	};
</script>
	</footer>
	</article>

	<div class="doc-view clearfix" style="margin-top: 5px;">
		<script type="text/javascript" language="javascript">
	function ifrmLoaded(hh, h) {
		$("#iFrame1").css("height", hh + h * 30);
	}
</script>
		<div id="viewer">
			<iframe id="iFrame1" name="iFrame1" align="top" scrolling="auto"
				frameborder=0 width="100%"
				src="<c:out value='${smvc }/view/doc-${document.ID }'/>.html"></iframe>
		</div>

	</div>
</div>
</div>
<div class="col-md-3 ">
	<div class="main-content" style="padding: 8px;">
		<div class="block block-block doc-upload">
			<h2 class="widget-title">
				文档上传
			</h2>
			<div class="widget-content">
				<a class="" href="admin/doc_add.html">上传文档</a>
			</div>
		</div>


		<div class="block block-block mod page-doc-list relate-doc">
			<h2 class="widget-title">
				相似文档
			</h2>
			<div class="widget-content">

				<ul>
					<c:forEach var="s" items="${similars}">
						<li>
							<a href="<c:out value='${smvc }/show/${s.ID}.html' />"
								target="_blank"> <c:out value="${s.title }"
									escapeXml="false"></c:out> </a>

						</li>

					</c:forEach>
				</ul>

			</div>
		</div>
		<div class="block block-block pinglun">
			<h2 class="widget-title">
				评论信息
			</h2>
			<div class="widget-content">
				<!-- 
				多说评论框 start -->
				<div class="ds-thread" data-thread-key="测试ID" data-title="测试文档"
					data-url="测试ID"></div>
				<!-- 多说评论框 end -->
				<!-- 多说公共JS代码 start (一个网页只需插入一次) -->
				<script type="text/javascript">
	var duoshuoQuery = {
		short_name : "bysj"
	};
	(function() {
		var ds = document.createElement('script');
		ds.type = 'text/javascript';
		ds.async = true;
		ds.src = (document.location.protocol == 'https:' ? 'https:' : 'http:')
				+ '//static.duoshuo.com/embed.js';
		ds.charset = 'UTF-8';
		(document.getElementsByTagName('head')[0] || document
				.getElementsByTagName('body')[0]).appendChild(ds);
	})();
</script>
				<!-- 多说公共JS代码 end -->

			</div>
		</div>

	</div>
</div>
</div>

</div>

<!-- 底部 -->
<jsp:include page="common-footer.jsp"></jsp:include>