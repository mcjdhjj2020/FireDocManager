<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />

<jsp:include page="common-head.jsp"></jsp:include>

<div class="region region-content">
	<ul class="search-results">
		<c:forEach var="d" items="${pDocument.documents }">
			<li>
				<article id="node-1344"
					class="node node-blog node-promoted node-teaser clearfix">
				<h1 property="dc:title" datatype="">
					<a href="<c:out value='${smvc }/show/${d.ID }.html' />" target="_blank"> <c:out
							value="${d.title }" escapeXml="false"></c:out> </a>
				</h1>

				<header class="node-header">
				<div class="title-bottom">

					<span class="icon-user"><c:out value="${d.uploaduser }"></c:out>
					</span>&nbsp;
					<span class="icon-calendar"><c:out value="${d.uploadtime }"></c:out>
					</span>&nbsp;
					<span class="icon-eye-open"><c:out value="${d.viewnum }"></c:out>
					</span>&nbsp;
					<span class="icon-download-alt"><c:out value="${d.downnum }"></c:out>
					</span>
				</div>
				<div class="article-type article-type-doc">
					<a href="" title="doc"> <i class="icon-blog"><c:out
								value="${d.type }"></c:out> </i> </a>
				</div>
				</header>

				<section class="body-content clearfix">
				<div
					class="field field-name-body field-type-text-with-summary field-label-hidden">
					<div class="field-items">
						<div class="field-item even" property="content:encoded">
							<p>
								<c:out value="${d.body }" escapeXml="false"></c:out>
							</p>
						</div>
					</div>
				</div>
				</section>


				<footer class="clearfix">
				<div
					class="field field-name-field-blog-tag field-type-taxonomy-term-reference field-label-hidden clearfix">
					<ul class="links">
						<i class="icon-tags" style="color: #cccccc;"></i>
						<li>
							<a href=""><c:out value="${d.roleid }"></c:out> </a>
						</li>
						<li>
							<a href=""><c:out value="${d.cate }"></c:out> </a>
						</li>
						<li>
							<a href="">计算机</a>
						</li>
						<li>
							<a href="">编程</a>
						</li>
					</ul>
				</div>
				<a href="<c:out value='${smvc }/show/${d.ID }.html' />"
					class="node_read_more ">阅读全文 <i class="icon-share-alt"></i> </a>
				</footer>



				</article>
			</li>
		</c:forEach>

	</ul>
</div>


<div class="item-list pager-item-list">
	<ul class="pagers">
	<!-- 
		<li class="first">
			1
		</li>
		<li class="pager-item">
			<a title="到第 2 页" href="/node?page=1">2</a>
		</li>
		<li class="pager-item">
			<a title="到第 3 页" href="/node?page=2">3</a>
		</li>
		<li class="pager-item">
			<a title="到第 4 页" href="/node?page=3">4</a>
		</li>
		<li class="pager-item">
			<a title="到第 5 页" href="/node?page=4">5</a>
		</li>
		<li class="pager-current pager-item">
			<a title="到第 6 页" href="/node?page=5">6</a>
		</li>
		<li class="pager-item">
			<a title="到第 7 页" href="/node?page=6">7</a>
		</li>
		<li class="pager-item">
			<a title="到第 8 页" href="/node?page=7">8</a>
		</li>
		<li class="pager-item">
			<a title="到第 9 页" href="/node?page=8">9</a>
		</li>
		<li class="pager-ellipsis">
			…
		</li>
		<li class="pager-next">
			<a class="icon-angle-right" title="去下一个页面" href="/node?page=1">下一页
				›</a>
		</li>
		<li class="pager-last last">
			<a class="icon-double-angle-right" title="到最后一页" href="/node?page=67">末页
				»</a>
		</li>
		 -->
	</ul>
</div>
<script type="text/javascript">
	$(function() {
		var currentPage='<c:out value="${pDocument.pageBean.page}"/>';
		var maxSize='<c:out value="${pDocument.pageBean.count/pDocument.pageBean.pnum+1 }"/>';
		maxSize=Math.floor(maxSize);
		currentPage=parseInt(currentPage);
		var pnum='<c:out value="${pDocument.pageBean.pnum}"/>';
		var count='<c:out value="${pDocument.pageBean.count}"/>';
		setPage("search.html",currentPage,maxSize,pnum,"","");
	});
	function setPage(action, currentPage, maxSize,pnum, suffix, preffix) {
		if (currentPage < 1)
			currentPage = 1;
		var x = currentPage;
		var maxPage = maxSize;
		var minPage = 1;
		var i = x % 10 == 0 ? (x - x % 10) : ((x - x % 10) + 1);
		var pages = new Array();
		var lisText = new Array();
		if (x != 1) {
			pages.push(x - 1);
			lisText.push('&laquo;')
		}
		if (x >= 10) {
			pages.push(minPage);
			lisText.push(minPage + "...");
		}
		if (i > 10)
			i -= 1;
		for ( var j = i; j <= i + 10 && j <= maxPage; j++) {
			pages.push(j);
			lisText.push(j);
		}
		if (i + 10 < maxPage) {
			pages.push(maxPage);
			lisText.push('...' + maxPage)
		}
		if (x != maxPage) {
			pages.push(x + 1);
			lisText.push('&raquo;')
		}
		for ( var i = 0; i < pages.length; i++) {
			if (pages[i] == null)
				continue;
			var href = action + "?page=" + (pages[i]);
			if (suffix)
				href = href + '&' + suffix;
			if (preffix)
				href += preffix += href;
			if (pages[i] == x)
				$(".pagers").append(
						"<li class='pager-current pager-item'><a  href='"+href+"'>"
								+ lisText[i] + "</a></li>");
			else
				$(".pagers").append(
						"<li class='pager-item'><a href='"+href+"'>"
								+ lisText[i] + "</a></li>");
		}
	}
</script>
</div>
<div class="col-md-3">
	456
</div>
</div>

</div>

<jsp:include page="common-footer.jsp"></jsp:include>
