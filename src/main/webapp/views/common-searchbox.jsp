<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="search-box">
 <div class="search-box-inner">
  <div class="search-type-list">
   <a to="simple-search" onclick="change(this)" href="javascript:void(0);">简单搜索</a>
   <a to="professional-search" onclick="change(this)" href="javascript:void(0);">专业搜索</a>
   <a to="advanced-search" onclick="change(this)" href="javascript:void(0);">高级搜索</a>
   <a class="icon-caret-down" onclick="changeopen(this)" href="javascript:void(0);"></a>
  </div>
  <div class="simple-search nocurrent">
   <form action="search.html" class="form-horizontal" role="form">
    <div class="form-group">
     <div class="col-sm-12">
      <span class="span-title">简单搜索</span>
     </div>
    </div>
    <div class="form-group">
     <div class="col-sm-10">
      <input type="text" name="keyword" placeholder="请输入搜索关键词，如 '课程'" class="form-control">
     </div>
    </div>
    <div class="form-group">
     <div class="col-sm-offset-10 col-sm-2">
      <input hidden name="home" value="nohome">
      <button type="submit" class="btn btn-primary">
       Submit
      </button>
     </div>
    </div>
   </form>
  </div>
  <div class="advanced-search current">
   <form action="search.html" class="form-horizontal" role="form">
    <div class="col-md-8">


    </div>
    <div class="form-group">
     <div class="col-sm-12">
      <span class="span-title">高级搜索</span>
     </div>
    </div>
    <div class="form-group">

     <div class="col-md-2 control-label">
      <span>检索项</span>
     </div>
     <div class="col-md-3 control-label">
      <span>逻辑检索词</span>
     </div>
    </div>
    <br />
    <div class="form-group">

     <div class="col-md-2 control-label">
      <span class="control-label">标题</span>
     </div>
     <div class="col-md-3">
      <input name="keyword" type="text" class="form-control">
     </div>
    </div>
    <div class="form-group">

     <div class="col-md-2 control-label">
      <span>内容</span>
     </div>
     <div class="col-md-3">
      <input name="body_keyword" type="text" class="form-control">
     </div>
     <div class="col-md-2">
      <select name="title_body" class="form-control">
       <option value="1">
        并且
       </option>
       <option value="0" selected>
        或
       </option>
      </select>
     </div>
    </div>
    <div class="form-group">

     <div class="col-md-2 control-label">
      <span>排序</span>

     </div>
     <div class="col-md-3">
      <select name="sortby" class="form-control">
       <option value="0" selected>
        匹配度
       </option>
       <option value="1">
        时间
       </option>
      </select>
     </div>
     <div class="col-md-2">
      <select name="sort" class="form-control">
       <option value="desc">
        倒序
       </option>
       <option value="asc" selected>
        正序
       </option>
      </select>
     </div>
    </div>
    <div class="form-group">

     <div class="col-md-2 control-label">
      <span>每页显示条数</span>

     </div>
     <div class="col-md-3">
      <select name="limit" class="form-control">
       <option value="10" selected>
        10
       </option>
       <option value="20">
        20
       </option>
       <option value="50">
        50
       </option>
      </select>
     </div>

    </div>
    <div class="form-group">
     <div class="col-md-offset-6">
      <input hidden name="home" value="nohome">
      <button type="submit" class="btn btn-primary">
       Submit
      </button>
     </div>
    </div>
   </form>
   <div class="ad-des">
    <p>
     标题：文档的标题（必填）
    </p>
    <p>
     内容：文档内容，选填；后边为逻辑词
    </p>
    <p>
     排序方式：支持按照匹配度，时间等进行排序
    </p>
    <p>
     每页最大显示条数为50
    </p>
   </div>

  </div>
  <div class="professional-search nocurrent">
   <form action="search.html" class="form-horizontal" role="form">
    <div class="form-group">
     <div class="col-sm-12">
      <span class="span-title">专业搜索</span>
     </div>
    </div>
    <div class="form-group">
     <div class="col-sm-10">
      <textarea placeholder="请输入搜索语句,如 title:课程" name="searchstring" class="form-control" rows="3"></textarea>
     </div>
    </div>
    <div class="form-group">
     <div class="col-sm-offset-10 col-sm-2">
      <input hidden name="home" value="nohome">
      <button type="submit" class="btn btn-primary">
       Submit
      </button>
     </div>
    </div>
    <input hidden name="home" value="nohome">
   </form>
  </div>
 </div>
</div>
<script type="text/javascript" language="javascript">
	function change(event) {
		var t = $(event).attr("to");
		$(".simple-search").removeClass("current").addClass("nocurrent");
		$(".professional-search").removeClass("current").addClass("nocurrent");
		$(".advanced-search").removeClass("current").addClass("nocurrent");
		$("." + t).removeClass("nocurrent").addClass("current");
		return false;
	}
	function changeopen(event) {
		var c = $(event).attr("class");
		if (c == "icon-caret-up") {
			$(".search-box").css({
				"height" : "50px",
				"overflow" : "hidden"
			});
			$(".ad-des").css("z-index", "-999");

			$(event).removeClass("icon-caret-up").addClass("icon-caret-down");
		} else {
			$(".search-box").removeAttr("style");
			$(".ad-des").removeAttr("style");
			$(event).removeClass("icon-caret-down").addClass("icon-caret-up");
		}
		return false;
	}
	function getQueryString(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
		var r = window.location.search.substr(1).match(reg);
		if (r != null)
			return unescape(r[2]);
		return null;
	}
	if (getQueryString("home") == "nohome") {
		$(".search-box").css({
			"height" : "50px",
			"overflow" : "hidden"
		});
		$(".ad-des").css("z-index", "-999");
	} else {

	}
</script>
