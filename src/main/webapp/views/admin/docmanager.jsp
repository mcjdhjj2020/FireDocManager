<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value='${pageContext.request.contextPath}'/>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<jsp:include page="common/admin-common-head.jsp"></jsp:include>
<jsp:include page="common/admin-common-sidebar.jsp"></jsp:include>
<div class="page-header">
	<h1>
		分类/文档管理
		<small> <i class="icon-double-angle-right"></i> 文档管理 </small>
	</h1>
</div>
<div class="dataTables_wrapper" role="grid">
	<div class="row" style="height: 43px;">
		<div class="col-sm-6">
			<div id="sample-table-2_length" class="dataTables_length">
				<label>
					每页展示
					<select size="1" name="sample-table-2_length"
						aria-controls="sample-table-2">
						<option value="10" selected="selected">
							10
						</option>
						<option value="25">
							25
						</option>
						<option value="50">
							50
						</option>
						<option value="100">
							100
						</option>
					</select>
					条记录
				</label>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="dataTables_filter" id="sample-table-2_filter">
				<label>
					搜索:
					<input type="text" aria-controls="sample-table-2">
				</label>
			</div>
		</div>
	</div>

	<table class="table table-striped table-bordered table-hover dataTable">
		<thead>
			<tr role="row">
				<th class="center sorting_disabled" role="columnheader" rowspan="1"
					colspan="1" style="width: 57px;" aria-label="">
					<label>
						<input type="checkbox" class="ace">
						<span class="lbl"></span>
					</label>
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					ID
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					名称
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					类型
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					页码
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					分类
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					归属
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					时间
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					用户
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					审核
					<input class="shenhe" type="checkbox" all="true">
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					索引
					<input class="index" type="checkbox" all="true">
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					查看
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					下载
				</th>
				<th role="columnheader" tabindex="0" aria-controls="sample-table-2"
					rowspan="1" colspan="1" aria-sort="ascending">
					操作
				</th>
			</tr>
		</thead>
		<c:forEach var="d" items="${docs }">
			<tr class="line">
				<td class="center  sorting_1">
					<label>
						<input type="checkbox" class="ace">
						<span class="lbl"></span>
					</label>
				</td>
				<td>
					<c:out value="${d.ID }"></c:out>
				</td>
				<td>
					<c:out value="${d.title }"></c:out>
				</td>
				<td>
					<c:out value="${d.type }"></c:out>
				</td>
				<td>
					<c:out value="${d.page }"></c:out>
				</td>
				<td>
					<c:out value="${d.cate }"></c:out>
				</td>
				<td>
					<c:out value="${d.roleid }"></c:out>
				</td>
				<td>
					<c:out value="${d.uploadtime }"></c:out>
				</td>
				<td>
					<c:out value="${d.uploaduser }"></c:out>
				</td>
				<td>
					<label>
						<c:choose>
							<c:when test="${d.shenhe==1}">
								<input did="<c:out value='${d.ID }'/>" class="shenhe"
									type="checkbox" checked all="false">
							</c:when>
							<c:otherwise>
								<input did="<c:out value='${d.ID }'/>" class="shenhe"
									type="checkbox" all="false">
							</c:otherwise>
						</c:choose>
					</label>
				</td>
				<td>

					<label>
						<c:choose>
							<c:when test="${d.indexed==1}">
								<input did="<c:out value='${d.ID }'/>" class="index"
									type="checkbox" checked all="false">
							</c:when>
							<c:otherwise>
								<input did="<c:out value='${d.ID }'/>" class="index"
									type="checkbox" all="false">
							</c:otherwise>
						</c:choose>
					</label>

				</td>
				<td>
					<c:out value="${d.viewnum }"></c:out>
				</td>
				<td>
					<c:out value="${d.downnum }"></c:out>
				</td>
				<td>
					<div
						class="visible-md visible-lg hidden-sm hidden-xs action-buttons">

						<a class="red" href="javascript:void(0);"> <i
							did="<c:out value='${d.ID }'/>"
							class="doc_delete icon-trash bigger-130"></i> </a>
					</div>
				</td>
			</tr>


		</c:forEach>


	</table>
	<div class="row" style="height: 57px;">
		<div class="col-sm-6">
			<div class="dataTables_info" id="sample-table-2_info">
				<c:out value="${(page.page-1)*page.pnum+1}"></c:out>
				- /
				<c:out value="${(page.page-1)*page.pnum+fn:length(docs)}"></c:out>
				共
				<c:out value="${page.count }"></c:out>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="dataTables_paginate paging_bootstrap">
				<ul class="pagination">
					<c:if test="${page.page<=1 }">
						<li class="prev disabled">
							<a
								href="<c:out value='?pnum=${page.pnum }&page=${page.page-1 }&searchword=${searchword }'/>">
								<i class="icon-double-angle-left"></i> </a>
						</li>
					</c:if>
					<c:if test="${page.page>1 }">
						<li class="prev">
							<a
								href="<c:out value='?pnum=${page.pnum }&page=${page.page-1 }&searchword=${searchword }'/>">
								<i class="icon-double-angle-left"></i> </a>
						</li>
						<li>
							<a
								href="<c:out value='?pnum=${page.pnum }&page=${page.page-1 }&searchword=${searchword }'/>"><c:out
									value="${page.page-1 }" /> </a>
						</li>
					</c:if>

					<li class="active">
						<a
							href="<c:out value='?pnum=${page.pnum }&page=${page.page }&searchword=${searchword }'/>"><c:out
								value="${page.page }" /> </a>
					</li>
					<c:if test="${page.page<(page.count/page.pnum) }">
						<li>
							<a
								href="<c:out value='?pnum=${page.pnum }&page=${page.page +1}&searchword=${searchword }'/>"><c:out
									value="${page.page+1 }" /> </a>
						</li>
						<li class="next">
							<a
								href="<c:out value='?pnum=${page.pnum }&page=${page.page+1 }&searchword=${searchword }'/>">
								<i class="icon-double-angle-right"></i> </a>
						</li>
					</c:if>
					<c:if test="${page.page>=(page.count/page.pnum) }">
						<li class="next disabled">
							<a
								href="<c:out value='?pnum=${page.pnum }&page=${page.page+1 }&searchword=${searchword }'/>">
								<i class="icon-double-angle-right"></i> </a>
						</li>
					</c:if>

				</ul>
			</div>
		</div>
	</div>


</div>
<script src="<c:out value='${smvc}'/>/assets/layer/layer.min.js"></script>
<script>
	$('.shenhe').on('click', function() {
		var ii = layer.load();
		var checked = $(this).is(':checked');
		var id = $(this).attr("did");
		var all = $(this).attr("all");
		console.log(checked + "::" + id);
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/doc_shenhe.html", {
			shenhe : checked,
			id : id,
			shenheall : all
		}, function(data) {
			layer.close(ii);
			if (checked) {
				layer.msg('审核成功？' + data.success, 1, -1); //2秒后自动关闭，-1代表不显示图标
			} else {
				layer.msg('取消审核成功？' + data.success, 1, -1); //2秒后自动关闭，-1代表不显示图标
			}
		}, "json");
	});
	$('.index').on('click', function() {
		var ii = layer.load();
		var checked = $(this).is(':checked');
		var id = $(this).attr("did");
		var all = $(this).attr("all");
		console.log(checked + "::" + id);
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/doc_index.html", {
			index : checked,
			id : id,
			indexall : all
		}, function(data) {
			layer.close(ii);
			if (checked) {
				layer.msg('索引成功？' + data.success, 1, -1); //2秒后自动关闭，-1代表不显示图标
			} else {
				layer.msg('取消索引成功？' + data.success, 1, -1); //2秒后自动关闭，-1代表不显示图标
			}
		}, "json");
	});
	$('.doc_delete').on('click', function(event) {
		var ii = layer.load();
		var id = $(this).attr("did");
		var par = $(this).closest('tr');
		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/doc_del.html", {
			docId : id
		}, function(data) {
			layer.close(ii);
			console.log(data.success);
			if(data.success){
				$(par).remove();
			}
		}, "json");

	});
</script>

<jsp:include page="common/admin-common-footer.jsp"></jsp:include>