<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />

<jsp:include page="../common/admin-common-head.jsp"></jsp:include>
<jsp:include page="../common/admin-common-sidebar.jsp"></jsp:include>
<div class="page-header">
 <h1>
  用户管理
  <small> <i class="icon-double-angle-right"></i> 修改个人信息 </small>
 </h1>
</div>
<!-- /.page-header -->
<div class="row">
 <div class="col-xs-12">
  <div class="row">
   <div class="form-horizontal" role="form" name="uedit_user_simple">

    <div class="col-sm-4">
     <br />
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       登陆账号
      </label>

      <div class="col-sm-9">
       <input type="text" name="username" id="form-field-1" class="col-xs-12 col-sm-12" value="<c:out value='${u.userName }'/>" readonly>
      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       旧密码
      </label>

      <div class="col-sm-9">
       <input type="text" name="psw_old" id="form-field-1" class="col-xs-12 col-sm-12" />

      </div>
     </div>
     <div class="space-4"></div>
     <div class="form-group">
      <label class="col-sm-3 control-label no-padding-right" for="form-field-1">
       新密码
      </label>

      <div class="col-sm-9">
       <input type="text" name="psw" id="form-field-1" class="col-xs-12 col-sm-12" />
      </div>
     </div>


     <div class="space-4"></div>

     <div class="clearfix">
      <div class="col-md-offset-3 col-md-9">
       <button class="btn btn-info" type="button submit" onclick="updateDocMeta(this);">
        <i class="icon-ok bigger-110"></i> 提交
       </button>
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
       <span style="color: red;" class="message"></span>
      </div>
     </div>

    </div>

   </div>
  </div>
 </div>
</div>
<script src="<c:out value='${smvc}'/>/assets/layer/layer.min.js"></script>
<script type="text/javascript">
	function updateDocMeta(event) {
		var ii = layer.load();
		var par = $(event).closest('div[name=uedit_user_simple]');
		var username = $(par).find("input[name=username]").attr("value");
		var psw_old = $(par).find("input[name=psw_old]").val();
		var psw = $(par).find("input[name=psw]").val();

		var contextPath = $("html").attr("basepath");
		$.post(contextPath + "/admin/user_edit_chpwd.html", {
			username : username,
			psw_old : psw_old,
			psw : psw
		}, function(data) {
			layer.close(ii);
			$(".message").text(data.success);

		}, "json");
	}
</script>
<jsp:include page="../common/admin-common-footer.jsp"></jsp:include>