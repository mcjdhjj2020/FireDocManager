<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />
<!DOCTYPE HTML>
<html lang="zh" basepath='<c:out value="${pageContext.request.contextPath }"></c:out>'>

	<head>
		<meta charset="utf-8" />
		<title>Dashboard - Ace Admin</title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- basic styles -->

		<link href="<c:out value='${smvc }'/>/assets/css/bootstrap.min.css"
			rel="stylesheet" />
		<link href="<c:out value='${smvc }'/>/assets/css/font-awesome.min.css"
			rel="stylesheet" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="<c:out value='${smvc }'/>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<!-- fonts -->

		<link rel="stylesheet"
			href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

		<!-- ace styles -->

		<link rel="stylesheet"
			href="<c:out value='${smvc }'/>/assets/css/ace.min.css" />
		<link rel="stylesheet"
			href="<c:out value='${smvc }'/>/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet"
			href="<c:out value='${smvc }'/>/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<c:out value='${smvc }'/>/assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		
		<script
			src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>

		<!-- ace settings handler -->

		<script src="<c:out value='${smvc }'/>/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="<c:out value='${smvc }'/>/assets/js/html5shiv.js"></script>
		<script src="<c:out value='${smvc }'/>/assets/js/respond.min.js"></script>
		<![endif]-->

	</head>

	<body>
		<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
	try {
		ace.settings.check('navbar', 'fixed')
	} catch (e) {
	}
</script>

			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand"> <small> <i
							class="icon-fire"></i> 管理后台 </small> </a>
					<!-- /.brand -->
				</div>
				<!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">

						<li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i
								class="icon-bell-alt icon-animated-bell"></i> <span
								class="badge badge-important">8</span> </a>

							<ul
								class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-warning-sign"></i> 8 条通知
								</li>

								<li>
									<a href="#">
										<div class="clearfix">
											<span class="pull-left"> <i
												class="btn btn-xs no-hover btn-pink icon-comment"></i>
												你的文档已经被审核成功 </span>
											<span class="pull-right badge badge-info">+12</span>
										</div> </a>
								</li>

								<li>
									<a href="#"> <i class="btn btn-xs btn-primary icon-user"></i>
										你的文档《xxx》被管理员删除 </a>
								</li>

								<li>
									<a href="#"> 查看所有的通知 <i class="icon-arrow-right"></i> </a>
								</li>
							</ul>
						</li>

						<li class="green">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i
								class="icon-envelope icon-animated-vertical"></i> <span
								class="badge badge-success">5</span> </a>

							<ul
								class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-envelope-alt"></i> 5 条消息
								</li>

								<li>
									<a href="#"> <img
											src="<c:out value='${smvc }'/>/assets/avatars/avatar.png"
											class="msg-photo" alt="Alex's Avatar" /> <span
										class="msg-body"> <span class="msg-title"> <span
												class="blue">Alex:</span> 上传了一个文档 </span> <span class="msg-time">
												<i class="icon-time"></i> <span>a moment ago</span> </span> </span> </a>
								</li>

								<li>
									<a href="#"> <img
											src="<c:out value='${smvc }'/>/assets/avatars/avatar3.png"
											class="msg-photo" alt="Susan's Avatar" /> <span
										class="msg-body"> <span class="msg-title"> <span
												class="blue">Susan:</span> 审核了一个文档
												文档名字为：基于lucene的全文搜索引擎。好好学习，天天向上 </span> <span class="msg-time">
												<i class="icon-time"></i> <span>20 minutes ago</span> </span> </span> </a>
								</li>

								<li>
									<a href="#"> <img
											src="<c:out value='${smvc }'/>/assets/avatars/avatar4.png"
											class="msg-photo" alt="Bob's Avatar" /> <span
										class="msg-body"> <span class="msg-title"> <span
												class="blue">Bob:</span> 什么什么文档被成功索引，可以使用 </span> <span
											class="msg-time"> <i class="icon-time"></i> <span>3:15
													pm</span> </span> </span> </a>
								</li>

								<li>
									<a href="inbox.html"> 查看所有记录 <i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle"> <img
									class="nav-user-photo"
									src="<c:out value='${smvc }'/>/assets/avatars/user.jpg"
									alt="Jason's Photo" /> <span class="user-info"> <small>欢迎,</small>
									超级管理员 </span> <i class="icon-caret-down"></i> </a>

							<ul
								class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#"> <i class="icon-cog"></i> 设置 </a>
								</li>

								<li>
									<a href="#"> <i class="icon-user"></i> 账号设置 </a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="#"> <i class="icon-off"></i> 退出 </a>
								</li>
							</ul>
						</li>
					</ul>
					<!-- /.ace-nav -->
				</div>
				<!-- /.navbar-header -->
			</div>
			<!-- /.container -->
		</div>