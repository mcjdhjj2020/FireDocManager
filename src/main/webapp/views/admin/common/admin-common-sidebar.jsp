<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}/admin/" />
<div class="main-container" id="main-container">
 <script type="text/javascript">
	try {
		ace.settings.check('main-container', 'fixed')
	} catch (e) {
	}
</script>
 <!-- /.main-container-inner -->
 <div class="main-container-inner">
  <a class="menu-toggler" id="menu-toggler" href="#"> <span class="menu-text"></span> </a>
  <!-- 侧边栏开始 -->
  <div class="sidebar" id="sidebar">
   <script type="text/javascript">
	try {
		ace.settings.check('sidebar', 'fixed')
	} catch (e) {
	}
</script>

   <div class="sidebar-shortcuts" id="sidebar-shortcuts">
    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
     <button class="btn btn-success">
      <i class="icon-signal"></i>
     </button>

     <button class="btn btn-info">
      <i class="icon-pencil"></i>
     </button>

     <button class="btn btn-warning">
      <i class="icon-group"></i>
     </button>

     <button class="btn btn-danger">
      <i class="icon-cogs"></i>
     </button>
    </div>

    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
     <span class="btn btn-success"></span>

     <span class="btn btn-info"></span>

     <span class="btn btn-warning"></span>

     <span class="btn btn-danger"></span>
    </div>
   </div>
   <!-- #sidebar-shortcuts -->

   <ul class="nav nav-list">
    <li class="active">
     <a href="<c:out value='${smvc }'/>home.html"> <i class="icon-dashboard"></i> <span class="menu-text">仪表盘</span> </a>
    </li>

    <li>
     <a href="<c:out value='${smvc }'/>doc_add.html"> <i class="icon-cloud-upload"></i> <span class="menu-text">文档上传</span> </a>
    </li>

    <li>
     <a href="#" class="dropdown-toggle"> <i class="icon-folder-open-alt"></i> <span class="menu-text">分类/文档管理</span> <b class="arrow icon-angle-down"></b> </a>

     <ul class="submenu">
      <li>
       <a href="<c:out value='${smvc }'/>cate.html"> <i class="icon-double-angle-right"></i> 分类管理 </a>
      </li>

      <li>
       <a href="<c:out value='${smvc }'/>doc_manager.html"> <i class="icon-double-angle-right"></i> 文档管理 </a>
      </li>



      <li>
       <a href="#" class="dropdown-toggle"> <i class="icon-double-angle-right"></i> Three Level Menu <b class="arrow icon-angle-down"></b> </a>

       <ul class="submenu">
        <li>
         <a href="#"> <i class="icon-leaf"></i> Item #1 </a>
        </li>

        <li>
         <a href="#" class="dropdown-toggle"> <i class="icon-pencil"></i> 4th level <b class="arrow icon-angle-down"></b> </a>

         <ul class="submenu">
          <li>
           <a href="#"> <i class="icon-plus"></i> Add Product </a>
          </li>

          <li>
           <a href="#"> <i class="icon-eye-open"></i> View Products </a>
          </li>
         </ul>
        </li>
       </ul>
      </li>
     </ul>
    </li>

    <li>
     <a href="<c:out value='${smvc }'/>doc_manager.html" class="dropdown-toggle"> <i class="icon-list"></i> <span class="menu-text">审核文档</span> </a>

    </li>

    <li>
     <a href="#" class="dropdown-toggle"> <i class="icon-group"></i> <span class="menu-text">用户权限管理</span> <b class="arrow icon-angle-down"></b> </a>
     <ul class="submenu">
      <li>
       <a href="<c:out value='${smvc }'/>user_manager.html"> <i class="icon-double-angle-right"></i> 注册用户管理 </a>
      </li>
      <li>
       <a href="<c:out value='${smvc }'/>user_manager_quickadd.html"> <i class="icon-double-angle-right"></i> 快速添加用户 </a>
      </li>
      <li>
       <a href="<c:out value='${smvc }'/>role.html"> <i class="icon-double-angle-right"></i> 权限管理 </a>
      </li>
     </ul>

    </li>

    <li>
     <a href="#" class="dropdown-toggle"> <i class="icon-user-md"></i> <span class="menu-text">个人中心</span> <b class="arrow icon-angle-down"></b> </a>

     <ul class="submenu">
      <li>
       <a href="<c:out value='${smvc }'/>user/profile.html"> <i class="icon-double-angle-right"></i> 个人信息维护 </a>
      </li>

      <li>
       <a href="<c:out value='${smvc }'/>user/chpwd.html"> <i class="icon-double-angle-right"></i> 修改密码 </a>
      </li>


     </ul>
    </li>

    <li>
     <a href="#" class="dropdown-toggle"> <i class="icon-cogs"></i> <span class="menu-text">系统设置</span> <b class="arrow icon-angle-down"></b> </a>

     <ul class="submenu">
      
      <li>
       <a href="<c:out value='${smvc }'/>/SystemSetting.html"> <i class="icon-double-angle-right"></i> 系统配置 </a>
      </li>
      <li>
       <a href="<c:out value='${smvc }'/>/systeminfo.html"> <i class="icon-double-angle-right"></i> 系统信息 </a>
      </li>
     </ul>
    </li>

    <li>
     <a href="widgets.html"> <i class="icon-list-alt"></i> <span class="menu-text">概述</span> </a>
    </li>

   </ul>
   <!-- /.nav-list -->

   <div class="sidebar-collapse" id="sidebar-collapse">
    <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
   </div>

   <script type="text/javascript">
	try {
		ace.settings.check('sidebar', 'collapsed')
	} catch (e) {
	}
</script>
  </div>
  <!-- 侧边栏结束 -->
  <!-- 主内容区开始 -->
  <div class="main-content">
   <!-- 面包屑开始 -->
   <div class="breadcrumbs" id="breadcrumbs">
    <script type="text/javascript">
	try {
		ace.settings.check('breadcrumbs', 'fixed')
	} catch (e) {
	}
</script>

    <ul class="breadcrumb">
     <li>
      <i class="icon-home home-icon"></i>
      <a href="#">Home</a>
     </li>
     <li class="active">
      Dashboard
     </li>
    </ul>
    <!-- .breadcrumb -->
   </div>
   <!-- 面包屑结束 -->
   <div class="page-content">