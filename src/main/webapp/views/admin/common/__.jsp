<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:forEach var="cat" items="${catetree }">
	<div class="tree-folder" style="display: block;">
		<div class="tree-folder-header">
			<i class="red icon-folder-open"></i>
			<div class="tree-folder-name" cateid='<c:out value="${cat.id}" />' parentid='<c:out value="${cat.parent}" />'>
				<c:out value="${cat.title}" />
			</div>
		</div>
		<div class="tree-folder-content" style="display: block;">
			<c:if test="${fn:length(cat.childs) > 0}">
				<!-- 如果有childen -->
				<c:set var="catetree" value="${cat.childs}" scope="request" />
				<!-- 注意此处，子列表覆盖treeList，在request作用域 -->
				<c:import url="common/__.jsp" />
				<!-- 这就是递归了 -->
			</c:if>

		</div>

	</div>
</c:forEach>