<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="smvc" value="${pageContext.request.contextPath}" />
</div>
</div>
<!-- 主内容区结束 -->
<div class="ace-settings-container" id="ace-settings-container">
	<div class="btn btn-app btn-xs btn-warning ace-settings-btn"
		id="ace-settings-btn">
		<i class="icon-cog bigger-150"></i>
	</div>

	<div class="ace-settings-box" id="ace-settings-box">
		<div>
			<div class="pull-left">
				<select id="skin-colorpicker" class="hide">
					<option data-skin="default" value="#438EB9">
						#438EB9
					</option>
					<option data-skin="skin-1" value="#222A2D">
						#222A2D
					</option>
					<option data-skin="skin-2" value="#C6487E">
						#C6487E
					</option>
					<option data-skin="skin-3" value="#D0D0D0">
						#D0D0D0
					</option>
				</select>
			</div>
			<span>&nbsp; Choose Skin</span>
		</div>

		<div>
			<input type="checkbox" class="ace ace-checkbox-2"
				id="ace-settings-navbar" />
			<label class="lbl" for="ace-settings-navbar">
				Fixed Navbar
			</label>
		</div>

		<div>
			<input type="checkbox" class="ace ace-checkbox-2"
				id="ace-settings-sidebar" />
			<label class="lbl" for="ace-settings-sidebar">
				Fixed Sidebar
			</label>
		</div>

		<div>
			<input type="checkbox" class="ace ace-checkbox-2"
				id="ace-settings-breadcrumbs" />
			<label class="lbl" for="ace-settings-breadcrumbs">
				Fixed Breadcrumbs
			</label>
		</div>

		<div>
			<input type="checkbox" class="ace ace-checkbox-2"
				id="ace-settings-rtl" />
			<label class="lbl" for="ace-settings-rtl">
				Right To Left (rtl)
			</label>
		</div>

		<div>
			<input type="checkbox" class="ace ace-checkbox-2"
				id="ace-settings-add-container" />
			<label class="lbl" for="ace-settings-add-container">
				Inside
				<b>.container</b>
			</label>
		</div>
	</div>
</div>
<!-- /#ace-settings-container -->


</div>
<!-- /.main-container-inner -->
<a href="#" id="btn-scroll-up"
	class="btn-scroll-up btn btn-sm btn-inverse"> <i
	class="icon-double-angle-up icon-only bigger-110"></i> </a>
</div>



<script src="<c:out value='${smvc }'/>/assets/js/bootstrap.min.js"></script>
<script src="<c:out value='${smvc }'/>/assets/js/typeahead-bs2.min.js"></script>


<!-- ace scripts -->

<script src="<c:out value='${smvc }'/>/assets/js/ace-elements.min.js"></script>
<script src="<c:out value='${smvc }'/>/assets/js/ace.min.js"></script>




</body>

</html>